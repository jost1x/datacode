import json
from datetime import datetime
from urllib import request
from urllib.parse import quote
import functions

measures = {
    "voltage": {
        "label": "voltage",
        "unit": "V"
    },
    "current": {
        "label": "Corriente",
        "unit": "A"
    },
    "energy_total": {
        "label": "Energia Total",
        "unit": "kWh"
    },
    "energy_day": {
        "label": "Energia Diaria",
        "unit": "kWh"
    },
    "active_power": {
        "label": "Potencia Activa",
        "unit": "W"
    }
}

devices = [
    {
        "id": "COOPEUMO-SMARTMETERSOCKET-1",
        "measures": ["active_power", "energy_total"],
        "name": "calefactor"
    },
    {
        "id": "COOPEUMO-SMARTMETERSOCKET-2",
        "measures": ["active_power", "energy_total"],
        "name": "humidificador"
    },
    {
        "id": "COOPEUMO-SMARTMETERSOCKET-4",
        "measures": ["active_power", "energy_total"],
        "name": "ventilador"
    }
]

device = devices[0].get("id")
measure = devices[0].get("measures")[0]
from_date = datetime(2019, 12, 3, 8, 24, 00)
to_date = datetime(2019, 12, 12, 18, 47, 00)


MinPower = 1

active_power_data = functions.get_json_by_data(device, "active_power", from_date, to_date, '4h', '0', 'mean')
energy_total_data = functions.get_json_by_data(device, "energy_total", from_date, to_date, '4h', '0', 'mean')

time = functions.get_position(active_power_data)["times"]
active_power = functions.get_position(active_power_data)["values"]
energy_total = functions.get_position(energy_total_data)["values"]

#Max = {
#    "date": x[y.index(max(y))],
#    "value": max(y)
#}

cycles = []

cycle = {}
startIndex = 0
endIndex = 0
lastEndIndex = 0

n_cycles = 0
for _x in range(len(time)):
        if(_x > 0 and active_power[_x] > MinPower and active_power[_x-1] < MinPower):
            cycle = {}
            cycle["Start"] = time[_x]
            startIndex = _x

        if(startIndex > 0 and _x is not (len(time)-1) and active_power[_x-1] > active_power[_x] and active_power[_x] > MinPower):
            cycle["End"] = time[_x]
            endIndex = _x-1
            cycle["MaxPower"] = max(active_power[startIndex:endIndex+1])
            if(n_cycles < 1):
                cycle["TotalEnergy"] = energy_total[endIndex] - energy_total[startIndex]
            else:
                cycle["TotalEnergy"] = energy_total[endIndex] - energy_total[lastEndIndex]
            cycle["TotalTime"] = ((cycle["End"] - cycle["Start"]).seconds)/(3600)
            lastEndIndex = endIndex
            cycles.append(cycle)
            n_cycles = n_cycles + 1

print(n_cycles)
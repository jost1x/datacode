import json
from datetime import datetime
from urllib import request
from urllib.parse import quote


def get_influx_url(device, measure, from_date, to_date, group = '1h', fill = '0', influxf = 'mean'):
    from_date = ("{0}s".format(datetime.timestamp(from_date))).replace(".0", "")
    to_date = ("{0}s".format(datetime.timestamp(to_date))).replace(".0", "")

    influx_query = "SELECT {6}(\"value\") FROM \"{3}\" WHERE (\"device\" = '{0}') AND time >= {1} and time <= {2} GROUP BY time({4}) fill({5})".format(
        device, from_date, to_date, measure, group, fill, influxf)
    influx_url = 'http://influx.domergy.cl/query?db=pird&q={0}&epoch=s'.format(quote(influx_query))
    return influx_url


def get_json_by_url(url):
    with request.urlopen(url) as json_url:
        data = json.loads(json_url.read().decode())
    return data

def get_json_by_data(device, measure, from_date, to_date, group = '1h', fill = '0', influxf = 'mean'):
    return get_json_by_url(get_influx_url(device, measure, from_date, to_date, group, fill, influxf))

def get_position(data):
    values = data["results"][0]["series"][0]["values"]
    x = [datetime.fromtimestamp(value[0]) for value in values]
    y = [value[1] for value in values]
    return {"times": x, "values": y}

import json
from datetime import datetime
from urllib import request
import matplotlib
from matplotlib import dates as mpl_dates
from urllib.parse import quote
import matplotlib.pyplot as plt

measures = {
    "voltage": {
        "label": "voltage",
        "unit": "V"
    },
    "voltage": {
        "label": "Voltaje",
        "unit": "V"
    },
    "current": {
        "label": "Corriente",
        "unit": "A"
    },
    "energy_total": {
        "label": "Energia Total",
        "unit": "kWh"
    },
    "energy_day": {
        "label": "Energia Diaria",
        "unit": "kWh"
    },
    "active_power": {
        "label": "Potencia Activa",
        "unit": "W"
    }
}

devices = [
    {
        "id": "COOPEUMO-SMARTMETERSOCKET-1",
        "measures": ["active_power", "energy_total"],
        "name": "calefactor"
    },
    {
        "id": "COOPEUMO-SMARTMETERSOCKET-2",
        "measures": ["active_power", "energy_total"],
        "name": "humidificador"
    },
    {
        "id": "COOPEUMO-SMARTMETERSOCKET-4",
        "measures": ["active_power", "energy_total"],
        "name": "ventilador"
    }
]



from_date = datetime(2019, 7, 17, 8, 24, 00)
to_date = datetime(2019, 7, 27, 18, 47, 00)


def saveFig(device, measure, from_date_datetime, to_date_datetime, extra):
    from_date = ("{0}s".format(datetime.timestamp(
        from_date_datetime))).replace(".0", "")
    to_date = ("{0}s".format(datetime.timestamp(
        to_date_datetime))).replace(".0", "")

    influx_query = "SELECT mean(\"value\") FROM \"{3}\" WHERE (\"device\" = '{0}') AND time >= {1} and time <= {2} GROUP BY time(60m) fill(0)".format(
        device, from_date, to_date, measure)
    influx_url = 'http://influx.domergy.cl/query?db=pird&q={0}&epoch=s'.format(
        quote(influx_query))

    with request.urlopen(influx_url) as json_url:
        data = json.loads(json_url.read().decode())
        values = data["results"][0]["series"][0]["values"]
        x = [datetime.fromtimestamp(value[0]) for value in values]
        y = [value[1] for value in values]

        l = measures.get(measure)

        plt.plot_date(x, y, "-")
        plt.ylabel(l["label"]+" ["+l["unit"]+"]")
        plt.suptitle(l["label"])
        plt.grid()
        fig = plt.gcf()
        fig.autofmt_xdate()
        fig.set_size_inches(8, 4.5)
        fig.savefig('./graph/{0}_{1}.png'.format(extra, measure), dpi=100)
        plt.clf()
        plt.cla()



for device in devices:
    _id = device["id"]
    _name = device["name"]
    for measure in device["measures"]:
        saveFig(_id, measure, from_date, to_date, _name)
